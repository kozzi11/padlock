import std.stdio;
import std.range : chunks;
import std.conv : to;
import std.array : appender;
import std.bitmanip;
import std.digest : toHexString;
void main()
{
	ubyte prevByte = 0x00;
	auto record = appender!(ubyte[])();
	immutable data = "7E02000044020072415553000200000000000000090285509C053EBB0800000000000019080907172833262A4D30302C32392C313033343630303031373631373233263030303030303030303030302623F47E";
	foreach (b; data.chunks(2))
	{
		auto c = b.to!ubyte(16);
		if (prevByte == 0x7d)
		{
			if (c == 0x02)
			{
				c = 0x7e;
			}
			else
			{
				c = prevByte;
			}
		}

		prevByte = c;
		if (c == 0x7d)
		{
			continue;
		}
		record.put(c);
	}

	auto rec = record.data;
	HeaderMessage header = HeaderMessage(rec);
	auto msg = rec[0 .. msgLen];
	//size_t crcPosition = msgLen +

	auto locMsg = LocationMessage(msg);
	writeln(locMsg);

}

struct HeaderMessage
{
	this(ubyte[] rec)
	{
		writeln(rec.read!ubyte);
		writeln(rec.read!(ushort, Endian.bigEndian));
		auto msgLen = rec.read!(ushort, Endian.bigEndian);
		writeln(msgLen);
		auto imei = rec[0 .. 6];
		rec = rec[6 .. $];
		writeln(imei.toHexString);
		writeln(rec.read!(ushort, Endian.bigEndian));
	}
}

struct LocationMessage
{
	this(ubyte[] msg)
	{
		alert = msg.read!(uint, Endian.bigEndian);
		status = msg.read!(uint, Endian.bigEndian);
		latitude = msg.read!(uint, Endian.bigEndian);
		longtitude = msg.read!(uint, Endian.bigEndian);
		altitude = msg.read!(ushort, Endian.bigEndian);
		speed = msg.read!(ushort, Endian.bigEndian);
		direction = msg.read!(ushort, Endian.bigEndian);
		time = msg[0..6].toHexString.idup;
	}

private:

	uint alert;
	uint status;
	uint latitude;
	uint longtitude;
	ushort altitude;
	ushort speed;
	ushort direction;
	string time;
}
